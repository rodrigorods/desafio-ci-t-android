package com.ciandt.challengeandroid.worldwonderapp.listeners;

import com.ciandt.challengeandroid.worldwonderapp.bean.ResponseEnvelope;

public interface OnLoadListener {

    void OnLoad(ResponseEnvelope envelope);
    void OnError(ResponseEnvelope envelope);

}
