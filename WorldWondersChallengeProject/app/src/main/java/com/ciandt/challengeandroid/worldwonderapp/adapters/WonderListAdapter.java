package com.ciandt.challengeandroid.worldwonderapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ciandt.challengeandroid.worldwonderapp.R;
import com.ciandt.challengeandroid.worldwonderapp.bean.Wonder;
import com.ciandt.challengeandroid.worldwonderapp.bean.WondersData;

import java.util.ArrayList;

public class WonderListAdapter extends BaseAdapter {

    private final ArrayList<Wonder> wonders;
    private LayoutInflater inflater;

    public WonderListAdapter(Context context, WondersData wonders) {
        this.wonders = wonders.getWondersArray();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.wonders.size();
    }

    @Override
    public Object getItem(int position) {
        return this.wonders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.wonders.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Wonder wonder = wonders.get(position);
        WonderViewHolder holder;

        if (convertView == null) {
            holder = new WonderViewHolder();
            convertView = inflater.inflate(R.layout.cell_wonders_list, parent, false);

            holder.nomeTv = (TextView) convertView.findViewById(R.id.cell_txt_wonder_name);
            holder.countryTv = (TextView) convertView.findViewById(R.id.cell_txt_wonder_country);
            holder.descriptionTv = (TextView) convertView.findViewById(R.id.cell_txt_wonder_description);

            convertView.setTag(holder);
        } else {
            holder = (WonderViewHolder) convertView.getTag();
        }

        holder.nomeTv.setText(wonder.getName());
        holder.countryTv.setText(wonder.getCountry());
        holder.descriptionTv.setText(wonder.getDescription());

        return convertView;
    }

    static class WonderViewHolder {
        TextView nomeTv;
        TextView countryTv;
        TextView descriptionTv;
    }
}
