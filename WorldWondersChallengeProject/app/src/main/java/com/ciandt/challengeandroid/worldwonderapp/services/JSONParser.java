package com.ciandt.challengeandroid.worldwonderapp.services;

import com.ciandt.challengeandroid.worldwonderapp.bean.LoggedUser;
import com.ciandt.challengeandroid.worldwonderapp.bean.ResponseEnvelope;
import com.ciandt.challengeandroid.worldwonderapp.bean.Wonder;
import com.ciandt.challengeandroid.worldwonderapp.bean.WondersData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser
{
    public static ResponseEnvelope<LoggedUser> ParseLoggedUser (String jsonString) throws JSONException
    {
        ResponseEnvelope<LoggedUser> envelope = new ResponseEnvelope<>();
        JSONObject jRoot = new JSONObject(jsonString);

        int responseCode = jRoot.getInt("codigo");
        envelope.setResponseCode(responseCode);

        if (responseCode == 0)
        {
            JSONObject dataObj = jRoot.getJSONObject("dados");

            String token = dataObj.getString("token_auth");
            String nome = dataObj.getString("nome");
            boolean isHabilitado = dataObj.getBoolean("habilitado");

            LoggedUser user = new LoggedUser(token, nome, isHabilitado);
            envelope.setData(user);
        }

        return envelope;
    }

    public static ResponseEnvelope<WondersData> ParseWonders (String jsonString) throws JSONException
    {
        WondersData wonders;
        ResponseEnvelope<WondersData> envelope = new ResponseEnvelope<>();
        envelope.setResponseCode(0);

        JSONObject jRoot = new JSONObject(jsonString);

        JSONArray jArray = jRoot.getJSONArray("data");
        wonders = new WondersData(jArray.length());

        for (int i = 0; i<jArray.length(); i++) {
            JSONObject jWonderObject = jArray.getJSONObject(i);

            int id = jWonderObject.getInt("id");
            String name = jWonderObject.getString("name");
            String country = jWonderObject.getString("country");
            String description = jWonderObject.getString("description");

            wonders.insert(new Wonder(id, name, country, description));
        }

        envelope.setData(wonders);

        return envelope;
    }
}
