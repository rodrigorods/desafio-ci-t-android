package com.ciandt.challengeandroid.worldwonderapp.userprefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.ciandt.challengeandroid.worldwonderapp.bean.LoggedUser;

public class UserPrefs {

    private static final String TABLE_USER       = "7(*HCBxPAO@*";
    private static final String FIELD_USER_NAME  = "pAxCP(*Y)@(8";
    private static final String FIELD_USER_TOKEN = "o*A7@OzIAU&X";
    private static final String FIELD_USER_HABIL = "AC(*Y!vPA@@s";


    public static void LogUser(Context context, LoggedUser user)
    {
        SharedPreferences sharedPref = context.getSharedPreferences(TABLE_USER, Context.MODE_PRIVATE);
        sharedPref.edit()
                .putString(FIELD_USER_NAME, user.getNome())
                .putString(FIELD_USER_TOKEN, user.getToken())
                .putBoolean(FIELD_USER_HABIL, user.isHabilitado())
                .commit();
    }

    public static boolean isUserLogger(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(TABLE_USER, Context.MODE_PRIVATE);
        String token = sharedPref.getString(FIELD_USER_TOKEN, "");
        return !token.isEmpty();
    }

    public static void LogOutUser(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(TABLE_USER, Context.MODE_PRIVATE);
        sharedPref.edit().clear().commit();
    }

}
