package com.ciandt.challengeandroid.worldwonderapp.utils;

import android.app.AlertDialog;
import android.content.Context;

public class Util {
    public static void ShowWarning(Context c, String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", null);
        builder.show();
    }
}
