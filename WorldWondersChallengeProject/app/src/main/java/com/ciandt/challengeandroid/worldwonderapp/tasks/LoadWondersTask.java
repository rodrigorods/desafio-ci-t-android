package com.ciandt.challengeandroid.worldwonderapp.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.ciandt.challengeandroid.worldwonderapp.R;
import com.ciandt.challengeandroid.worldwonderapp.bean.ResponseEnvelope;
import com.ciandt.challengeandroid.worldwonderapp.bean.WondersData;
import com.ciandt.challengeandroid.worldwonderapp.listeners.OnLoadListener;
import com.ciandt.challengeandroid.worldwonderapp.services.WorldWondersServices;

import org.json.JSONException;

import java.io.IOException;

public class LoadWondersTask extends AsyncTask<Void, Void, ResponseEnvelope<WondersData>>
{
    private Context context;
    private ProgressDialog dialog;
    private OnLoadListener listener;

    public LoadWondersTask(Context context, OnLoadListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage(context.getString(R.string.warning_logging_in));
        dialog.show();
    }

    @Override
    protected ResponseEnvelope<WondersData> doInBackground(Void... params) {
        try {
            return WorldWondersServices.GetWondersList();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(ResponseEnvelope<WondersData> envelope) {
        super.onPostExecute(envelope);

        dialog.dismiss();

        if (envelope == null || envelope.getResponseCode() != 0) {
            listener.OnError(envelope);
        } else {
            listener.OnLoad(envelope);
        }
    }
}
