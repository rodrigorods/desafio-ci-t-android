package com.ciandt.challengeandroid.worldwonderapp.bean;

public class ResponseEnvelope<T>
{
    public static final int RESPONSE_OK = 0;
    public static final int RESPONSE_ERROR_INVALID_CREDENTIALS = 1;

    private int responseCode = -1;
    private T data;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }
}
