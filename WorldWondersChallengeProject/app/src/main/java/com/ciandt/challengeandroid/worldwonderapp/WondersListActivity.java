package com.ciandt.challengeandroid.worldwonderapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.ciandt.challengeandroid.worldwonderapp.adapters.WonderListAdapter;
import com.ciandt.challengeandroid.worldwonderapp.bean.ResponseEnvelope;
import com.ciandt.challengeandroid.worldwonderapp.bean.WondersData;
import com.ciandt.challengeandroid.worldwonderapp.listeners.OnLoadListener;
import com.ciandt.challengeandroid.worldwonderapp.tasks.LoadWondersTask;
import com.ciandt.challengeandroid.worldwonderapp.userprefs.UserPrefs;
import com.ciandt.challengeandroid.worldwonderapp.utils.Util;

public class WondersListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wonders_list);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new LoadWondersTask(this, onLoadListener).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_wonders_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       int id = item.getItemId();

        if (id == R.id.action_logout) {
            UserPrefs.LogOutUser(getBaseContext());
            startActivity(new Intent(getBaseContext(), LoginActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    OnLoadListener onLoadListener = new OnLoadListener() {
        @Override
        public void OnLoad(ResponseEnvelope envelope) {
            WondersData wonders = (WondersData) envelope.getData();
            ListView listView = (ListView) findViewById(R.id.wonders_list);
            WonderListAdapter adapter = new WonderListAdapter(getBaseContext(), wonders);

            listView.setAdapter(adapter);
        }

        @Override
        public void OnError(ResponseEnvelope envelope) {
            if (envelope == null) {
                Util.ShowWarning(WondersListActivity.this, "Um erro desconhecido impediu esta tarefa! Por favor tente novamente mais tarde!");
            }
        }
    };
}
