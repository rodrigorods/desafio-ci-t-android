package com.ciandt.challengeandroid.worldwonderapp.bean;

public class Wonder {

    private int id;
    private String name;
    private String description;
    private String country;

    public Wonder(int id, String name, String country, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCountry() {
        return country;
    }
}
