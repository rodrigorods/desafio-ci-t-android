package com.ciandt.challengeandroid.worldwonderapp.bean;

public class LoggedUser {

    private final boolean habilitado;
    private final String nome;
    private final String token;

    public LoggedUser(String token, String nome, boolean isHabilitado)
    {
        this.token = token;
        this.nome = nome;
        this.habilitado = isHabilitado;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public String getNome() {
        return nome;
    }

    public String getToken() {
        return token;
    }
}
