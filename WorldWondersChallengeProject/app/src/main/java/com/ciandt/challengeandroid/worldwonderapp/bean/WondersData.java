package com.ciandt.challengeandroid.worldwonderapp.bean;

import java.util.ArrayList;

public class WondersData {
    private ArrayList<Wonder> wonders;

    public WondersData (int size) {
        wonders = new ArrayList<>(size);
    }

    public void insert(Wonder wonder) {
        wonders.add(wonder);
    }

    public ArrayList<Wonder> getWondersArray() {
        return wonders;
    }
}
