package com.ciandt.challengeandroid.worldwonderapp.services;

import com.ciandt.challengeandroid.worldwonderapp.bean.LoggedUser;
import com.ciandt.challengeandroid.worldwonderapp.bean.ResponseEnvelope;
import com.ciandt.challengeandroid.worldwonderapp.bean.WondersData;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WorldWondersServices
{
    private static final String BASE_URL = "http://private-anon-b98a6a914-worldwondersapi.apiary-mock.com";

    private static final String LOGIN_METHOD = "/api/v1/login";
    private static final String LOGIN_PARAMS = "?email=%s&senha=%s";

    private static final String GET_WONDERS_METHOD = "/api/v1/worldwonders";

    public static ResponseEnvelope<LoggedUser> LogInApp(String email, String pass) throws IOException, JSONException {
        String params = String.format(LOGIN_PARAMS, email, pass);
        URL url = new URL(BASE_URL + LOGIN_METHOD + params);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);

        conn.connect();
        int response = conn.getResponseCode();

        if (response == HttpURLConnection.HTTP_OK) {
            InputStream inputStream = conn.getInputStream();
            String responseString = StreamToString(inputStream);
            ResponseEnvelope<LoggedUser> env = JSONParser.ParseLoggedUser(responseString);

            inputStream.close();
            conn.disconnect();

            return env;
        } else {
            conn.disconnect();
            return new ResponseEnvelope();
        }
    }

    public static ResponseEnvelope<WondersData> GetWondersList() throws IOException, JSONException {
       URL url = new URL(BASE_URL + GET_WONDERS_METHOD);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);

        conn.connect();
        int response = conn.getResponseCode();

        if (response == HttpURLConnection.HTTP_OK) {
            InputStream inputStream = conn.getInputStream();
            String responseString = StreamToString(inputStream);
            ResponseEnvelope<WondersData> env = JSONParser.ParseWonders(responseString);

            inputStream.close();
            conn.disconnect();

            return env;
        } else {
            conn.disconnect();
            return new ResponseEnvelope();
        }
    }

    private static String StreamToString(InputStream stream) throws IOException
    {
        BufferedReader r = new BufferedReader(new InputStreamReader(stream));
        StringBuilder total = new StringBuilder(stream.available());
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }

        return total.toString();
    }
}
