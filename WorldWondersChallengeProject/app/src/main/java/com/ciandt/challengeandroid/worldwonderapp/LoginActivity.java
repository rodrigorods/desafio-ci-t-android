package com.ciandt.challengeandroid.worldwonderapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ciandt.challengeandroid.worldwonderapp.bean.LoggedUser;
import com.ciandt.challengeandroid.worldwonderapp.bean.ResponseEnvelope;
import com.ciandt.challengeandroid.worldwonderapp.listeners.OnLoadListener;
import com.ciandt.challengeandroid.worldwonderapp.tasks.LogIntoSystemTask;
import com.ciandt.challengeandroid.worldwonderapp.userprefs.UserPrefs;
import com.ciandt.challengeandroid.worldwonderapp.utils.Util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private EditText userEdt;
    private EditText passEdt;

    private Pattern pattern;
    private Matcher matcher;
    private ConnectivityManager connectivityManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        if (UserPrefs.isUserLogger(getBaseContext())) {
            GoToWondersListActivity();
        }

        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        pattern = Pattern.compile(EMAIL_PATTERN);

        userEdt = (EditText) findViewById(R.id.login_edt_username);
        passEdt = (EditText) findViewById(R.id.login_edt_password);

        Button logInBtn = (Button) findViewById(R.id.login_btn_logging);
        logInBtn.setOnClickListener(logInClickListener);
    }

    private boolean isValidEmail(String matchStr){
        matcher = pattern.matcher(matchStr);
        return matcher.matches();
    }

    private boolean CanProceedToRequest(String email, String pass){
        if (email.isEmpty()) {
            userEdt.setError(getString(R.string.Obligatory_field));
            return false;
        } else if (pass.isEmpty()) {
            passEdt.setError(getString(R.string.Obligatory_field));
            return false;
        } else {
            if (!isValidEmail(email)) {
                userEdt.setError(getString(R.string.warning_invalid_email));
                return false;
            } else {

                NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();

                if (isConnected) {
                    return true;
                } else {
                    Toast.makeText(getBaseContext(), getString(R.string.no_connection), Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        }
    }

    Button.OnClickListener logInClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            String email = userEdt.getText().toString().trim();
            String pass = passEdt.getText().toString().trim();
            if (CanProceedToRequest(email, pass))
            {
                new LogIntoSystemTask(LoginActivity.this, email, pass, onLoadListener).execute();
            }
        }
    };

    OnLoadListener onLoadListener = new OnLoadListener() {
        @Override
        public void OnLoad(ResponseEnvelope envelope) {
            LoggedUser user = (LoggedUser) envelope.getData();
            String nomeUsuario = user.getNome();

            UserPrefs.LogUser(LoginActivity.this, user);
            Toast.makeText(getBaseContext(), getString(R.string.ola_usuario, nomeUsuario), Toast.LENGTH_LONG).show();

            GoToWondersListActivity();
        }

        @Override
        public void OnError(ResponseEnvelope envelope) {
            if (envelope == null) {
                Util.ShowWarning(LoginActivity.this, "Um erro desconhecido impediu esta tarefa! Por favor tente novamente mais tarde!");
            } else if (envelope.getResponseCode() == 1) {
                Toast.makeText(getBaseContext(), R.string.invalid_credentials, Toast.LENGTH_LONG).show();
            }
        }
    };

    private void GoToWondersListActivity() {
        startActivity(new Intent(getBaseContext(), WondersListActivity.class));
        finish();
    }

}
